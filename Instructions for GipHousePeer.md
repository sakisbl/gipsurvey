# Get Redmine project up and running using Vagrant

First, what is vagrant?
Vagrant is a command line tool with which you can define a virtual machine's setup using what is called a `Vagrantfile`. This file will tell vagrant how to setup a VM.

One of the advantages is that using vagrant we can guarantee that all the developers are using the same system with the same packages and OS. Another advantage is that we can check our Vagrant files into version control and this way a. keep making sure the developers all have the same setup and b. new developers and computers are introduced to the system much quicker.

## First things first; Install Vagrant and VirtualBox
Get the latest version of VirtualBox here:
https://www.virtualbox.org/wiki/Downloads

Get the latest version of Vagrant here:
http://downloads.vagrantup.com/tags/v1.3.1

Install both of them.

## Get the code and run vagrant
Get a checkout of the code that you want to run (in this case our project's code) through git.

```
git clone …
cd repo-name
./puppet/setup.sh # Otherwise copy puppet/database.yml to config/
```

Now get the VM up and running:
```
vagrant up
```

Yep that was it.

## Get Rails ready for running
In your project folder run:

```
vagrant ssh
```

You will now have an SSH session inside of the VM.

```
# CD into the project's code:
cd code
# Install the gems
bundle install
# Run the migrations
rake db:migrate
# Run the dev server
rails s
```

Now on your host machine go to:

```
http://localhost:3000
```

Done.